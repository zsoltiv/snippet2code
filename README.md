# snippet2code
A tiny neovim plugin that adds the ability to expand snippets to code written in Lua.
This plugin has no dependencies, and only defines a single function `snippet2code#SnippetToCode()`.

## Installation
### Plug
`Plug 'https://gitlab.com/zsoltiv/snippet2code.git'`

## Usage
define the `g:CodeSnippets` dictionary in your `init.vim` in the form `'snippet': ['line 1', 'line 2', '...']`.
## Example config
```
let g:CodeSnippets = {
\ 'html': [
\   '<!DOCTYPE html>',
\   '<html lang="en">',
\   '<head>',
\   '    <meta charset="UTF-8">',
\   '    <meta name="viewport" content="width=device-width, initial-scale=1.0">',
\   '    <title>Document</title>',
\   '</head>',
\   '<body>',
\   '',
\   '</body>',
\   '</html>',
\ ],
\}
```

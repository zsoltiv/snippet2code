local M = {}

function M.SnippetToCode()
    -- if the global CodeSnippets variable is defined
    if vim.g.CodeSnippets then
        -- get the word under the cursor
        local word = vim.api.nvim_eval('expand("<cword>")')

        for key,val in pairs(vim.g.CodeSnippets) do
            if key == word then
                -- delete the snippet
                vim.api.nvim_feedkeys('diw', 'nx', true)
                -- write the lines to the buffer
                vim.api.nvim_put(val, '', false, false)
            end
        end
    end
end

return M

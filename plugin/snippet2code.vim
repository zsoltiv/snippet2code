" prevent loading file twice
if exists('g:loaded_snippet2code') | finish | endif
let g:loaded_snippet2code = 1

" the plugin exposes a single command which
" calls SnippetToCode() from the lua file
command SnippetToCode :call snippet2code#SnippetToCode()
